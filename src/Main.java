import com.myphonebook.example.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

    Contact myContact = new Contact();
    myContact.setName("John Doe");
    myContact.setContactNumber("+639152468596");
    myContact.setAddress("Quezon City");

    System.out.println(myContact.getName());
    System.out.println(myContact.getContactNumber());
    System.out.println(myContact.getAddress());

    myContact.setName("Jane Doe");
    myContact.setContactNumber("+639162148573");
    myContact.setAddress("Caloocan City");

    System.out.println(myContact.getName());
    System.out.println(myContact.getContactNumber());
    System.out.println(myContact.getAddress());
    }
}
        /*
        ArrayList<String> contactDetails = new ArrayList<>();
        HashMap<String, String> myEntry = new HashMap<>();
        myEntry.put("Name","John Doe");
        myEntry.put("Contact Number", "+639152468596");
        myEntry.put("Address", "Quezon City");
        contactDetails.add(String.valueOf(myEntry));
        myEntry.put("Name","Jane Doe");
        myEntry.put("Contact Number", "+639162148573");
        myEntry.put("Address", "Caloocan City");

        contactDetails.add(String.valueOf(myEntry));

        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter Contact to see details:");
        String myInput = userInput.nextLine();
        for(int count = 0; count<contactDetails.size(); count++){
            System.out.println(contactDetails.get(count));
           }
        };
*/
